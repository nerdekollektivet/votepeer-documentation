# VotePeer Documentation

Censorship resistant on-chain blockchain voting. This is the documentation for the VotePeer project. See why [VotePeer is an improvement over traditional online voting](about.md).

See [screenshots and demos](demo.md).

## Research

#### [Two option vote contract for Bitcoin Cash](two-option-vote-contract.md) (2020)

#### [Voting on Bitcoin Cash using Traceable Ring Singatures](ring-signature-vote-contract.md) (2020)

#### [Transaction Input Payload Contract](input-payload-contract.md) (2021)

#### [Publishing Data on Bitcoin Cash Anonymously Using Faucets](anonymous-contract-funding.md) (2021)

#### [Multi option vote contract for Bitcoin Cash](multi-option-vote-contract.md) (2021)

## Developer Reference

#### [VotePeer API](votepeer-api.md)

#### [libbitcoincash reference](https://bitcoinunlimited.gitlab.io/libbitcoincashkotlin/dokka/)

#### [tallytree reference](https://bitcoinunlimited.gitlab.io/tallytree/tallytree/)

## Software

#### [VotePeer Android App](https://voter.cash/)

App for participating in elections. [Git repository](https://gitlab.com/nerdekollektivet/buip129-android).

#### [VotePeer Website](https://voter.cash/)

Web interface for creating and administering elections. [Git repository](https://gitlab.com/nerdekollektivet/buip129-website)

#### [TallyTree Library](https://gitlab.com/bitcoinunlimited/tallytree)

An implementation of [Merkle Tally Tree in Rust](http://bitcoinunlimited.net/blockchain_voting). Merkle Tally Trees
provide an efficent tally of an electronic vote. It scales to millions of
votes, while still being able to prove efficently to every voter that the
tally was fair.

#### [VotePeer Android Library](https://gitlab.com/nerdekollektivet/votepeer-library)

Library for integrating blockchain voting in Android projects.

#### [libbitcoincashkotlin](https://gitlab.com/bitcoinunlimited/libbitcoincashkotlin)

Bitcoin Cash library in kotlin (with batteries included philosophy). Low-level VotePeer functionality is implemented here, including ring signature primitives and Script contracts.

#### [WallyWallet](https://gitlab.com/wallywallet/android)

Bitcoin Cash Wallet with VotePeer integrated.

## Contact

*   [Telegram](http://t.me/buip129)
*   [Twitter](https://twitter.com/votepeer)
