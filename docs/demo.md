# Demo

Screenshots and demos of VotePeer user interface. See also the [VotePeer website](https://voter.cash)

## Demo: Anonymous vote on Bitcoin Cash

[![VotePeer YouTube Demo](http://img.youtube.com/vi/NBhE_v6yewo/0.jpg)](http://www.youtube.com/watch?v=NBhE_v6yewo "VotePeer: Anonymous vote on Bitcoin Cash demo")

## Demo: Transparent vote on Bitcoin Cash

| Election feed | Election |
| ------ | ------ |
| <img src="./election_list.jpg" width="420"/> | <img src="./election_detail.jpg" width="420"/> |
| Your Elections. | This has voted. |
