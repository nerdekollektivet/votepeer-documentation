# Bitcoin Unlimited member VotePeer identity confirmation

To verify your VotePeer identity for casting on-chain votes in the BU elections,
the following is suggested:

*   Compose a short message confirming your VotePeer identity.
*   Sign this message with your Bitcoin Unlimited membership address.
*   Post signature, message and address in [this forum thread](https://forum.bitcoinunlimited.info/t/confirm-your-votepeer-identity-here/219).

Example template for message:

`For BU votes in 2022, I will use the following VotePeer identity: bitcoincash:qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq`

It is implied that the message can be revoked with a new signed message.

## Signing message using Bitcoin Unlimited node

![Locate signing tool in menu](bu-sign-bitcoinunlimited-menu.png)
![Sign a message](bu-sign-bitcoinunlimited-sign.png)

## Signing message using Electron Cash

![Locate signing tool in menu](bu-sign-electron-cash-menu.png)
![Sign a message](bu-sign-electron-cash-sign.png)
