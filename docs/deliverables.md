# VotePeer deliverables

    Version 1.0

    Change log:
    - 1.0: Initial document.

This documents the deliverables of [BUIP129 (aka VotePeer)](https://github.com/BitcoinUnlimited/BUIP/blob/master/129.md).

## Glossary

*   **Blockchain Identity** - This private/public key pair representing a user. Usually identified as a P2PKH address. When voting, user signs the vote with his private key, while the signature is validated by anyone with the public key.
*   **P2PKH address** - An Bitcoin Cash address is normally represented by either a P2PKH or P2SH contract. A P2PKH contract is a standard, simple contract, where a user signs a transaction with his private key to prove ownership of coins.

## VotePeer Documentation

> Repository: <https://gitlab.com/bitcoinunlimited/votepeer-documentation>
>
> Hosted: <https://docs.voter.cash/>
>
> License: MIT

The primary source for all documentation on the VotePeer project. This includes research documentation, user manuals, FAQ, API functions, pilot run reports and this document.

Documentation is written in markdown format. The documentation website is updated with every change to the repository.

## Papers

> Repository: TODO
>
> License: MIT

Research on the two-option-vote contract has been written in a journal compatible format for publication.

## VotePeer web portal

> Repository: <https://gitlab.com/bitcoinunlimited/votepeer-web>
>
> Hosted: <https://voter.cash>
>
> License: MIT

Web portal for creating votes, viewing votes and managing participant
lists.

*   Users authenticate using the
    [BCH Identity Protocol](https://www.bitcoinunlimited.net/identity__protocol)
    from the VotePeer app. The web portal does not contain any sign-up process.
    Viewing public elections does not require authentication.

*   Votes can be private or public. When public, anyone can view vote
    details. When private, authenticated users can view votes they
    participate in or host.

*   The web portal queries the Bitcoin Cash blockchain to present the votes and
    final tally.

*   Users can create list of vote participants so they can be batch added
    to new votes.

*   Blockchain contract for vote can be selected. Different contracts
    support different vote properties, including transparent vs. anonymous
    participation, scalability, cost to vote etc.

*   The web portal also has the responsibility to push out app notifications
    when a user is added to an vote.

#### Frontend

The website uses the [Vue.js](https://vuejs.org/) framework and [Bootstrap](https://bootstrap-vue.org/) front-end library.

The front end is deployed as a static website. In our deployment, we use Gitlab pages and deploy it as part of CI/CD on merges to the master branch. It is hosted at <https://voter.cash>.

#### Backend

The backend provides a REST API backend. The backend API is documented using OpenAPI standard here: <https://docs.voter.cash/votepeer-api/>. The API documentation also includes URL to hosted server.

The API is deployed as [Firebase Cloud Functions](https://firebase.google.com/docs/functions), but written to be compatible with [Express](https://expressjs.com/) web framework for portability with other deployment options.

The backend API used by the frontend, the android app and integration on other websites, including the Bitcoin Unlimited website.

### VotePeer Android Library

> Repository: <https://gitlab.com/bitcoinunlimited/votepeer-android-app>
>
> Hosted: Self-hosted maven repository.
>
> License: MIT

Library for casting votes on-chain on Bitcoin Cash.

The library is developed to be a modular way to plug-in a functionally complete user interface for voters. The plugin only requires a private key as input parameter when initializing.

Features:

*   Authenticate using users *blockchain identity* with the [BCH Identity Protocol](https://www.bitcoinunlimited.net/identity__protocol).

*   List votes the user is participating in. List includes voting state; if user can vote, if they have voted and if so, what they voted on. This is done by querying the blockchain.

*   View election details, including the users own vote.

*   Cast transparent on-chain votes using the [two-option-vote](two-option-vote-contract.md) and [multi-option-vote](multi-option-vote-contract.md). The library also supports tallying these contracts by querying the blockchain directly.

*   Cast anonymous on-chain votes using the [ring-signature-vote-contract](ring-signature-vote-contract.md). The library also supports tallying this contract by querying the blockchain directly.

*   A user can view and share their blockchain identity.

*   Library is provided in a maven repository as a pre-built version for integrating it without needing to compile the library.

*   Receive push notification when added as participant to a vote.

### VotePeer Android Application

> Repository: <https://gitlab.com/bitcoinunlimited/votepeer-android-app>
>
> Google Play: <https://play.google.com/store/apps/details?id=info.bitcoinunlimited.voting>
>
> License: MIT

VotePeer Android app casting votes on-chain on Bitcoin Cash. Most of the functionality of this app is implemented in `votepeer-library`, this is the "skeleton" on top.

The app stores the *blockchain identity* of the user (aka private key). For new installations, a unique mnemonic is created at first startup. It is also possible to recover an identity by supplying a mnemonic phrase or a private key.

See section on the android library for feature list.

## Library: @bitcoinunlimited/votepeerjs

![logo](https://gitlab.com/bitcoinunlimited/votepeerjs/-/raw/master/contrib/logo-256x256.png)

> Repository: <https://gitlab.com/bitcoinunlimited/votepeerjs>
>
> NPM: <https://www.npmjs.com/package/@bitcoinunlimited/votepeerjs>
>
> License: MIT

Javascript library with support, or partial support for VotePeer contracts. This library was developed for use with the VotePeer web interface, VotePeer API backend and for research.

The following vote contract support is added to this library.

| Vote contract | Tally votes | Validate votes | Casting vote  |
|---------------|-------------|----------------|---------------|
|[two-option-vote](two-option-vote-contract.md)        |✅|✅|✅|
|[multi-option-vote](multi-option-vote-contract.md)    |✅|✅|❌|
|[ring-signature-vote](ring-signature-vote-contract.md)|✅|✅|❌|

Additionally the library has support for the non-vote contract [input payload contract](input-payload-contract.md) and querying Electrum servers for blockchain data.

### Library: @bitcoinunlimited/bchidentity

![logo](https://gitlab.com/bitcoinunlimited/bchidentity-js/-/raw/master/contrib/logo-256x256.png)

> Repository: <https://gitlab.com/bitcoinunlimited/bchidentity-js>
>
> Reference: <https://bitcoinunlimited.gitlab.io/bchidentity-js>
>
> NPM package: <https://www.npmjs.com/package/@bitcoinunlimited/bchidentity>

A javascript library for authenticating with the BCH Identity Protocol. The module authenticates against a bchidentity backend.

It was developed to login to <https://voter.cash> without needing to use a android device. It works with any project using BCH identity to authenticate.

The library comes with an example for authenticating using terminal client.

The CI/CD deploys a new version of the library on merges to the `master` branch. It also deploys updated reference documentation .

The library comes with good unit test coverage.

## Library: tallytree

<img alt="Merkle Tally Tree Icon" src="https://gitlab.com/bitcoinunlimited/tallytree/-/raw/main/contrib/tallytree.png" height="300px" width="auto">

> Repository: <https://gitlab.com/bitcoinunlimited/tallytree>
>
> License: MIT
>
> Crate: <https://crates.io/crates/tallytree>
>
> Reference: <https://bitcoinunlimited.gitlab.io/tallytree/tallytree/>

[Merkle Tally Tree](http://bitcoinunlimited.net/blockchain_voting) provide an efficient tally of an electronic vote. It scales to millions of votes, while still being able to prove efficiently to every voter that the tally was fair.

Tally trees are used in the [multi-option-vote](multi-option-vote-contract.md). This library is the research & reference implementation of it written in Rust.

The library is published to creates.io.

## Library: libbitcoincash improvements

As part of development of this project, the Kotlin library libbitcoincash has been improved in the following ways:

*   The library has been fully separated from the WallyWallet implementation.
    Any dependencies between the projects were resolved.

*   A gradle build script was developed for the library. This made it possible
    to compile the library independently of WallyWallet. The Kotlin DSL variant
    is used for gradle.

*   Previously manual steps for building the library were automated as part of
    the build script. This includes downloading Bitcoin Unlimited, downloading
    boost, extracting the source code and running the autogen tools.

*   A build step to produce build maven package was added. This allows for
    applications, including WallyWallet to include pre-built version of the
    library as dependency.

*   The contracts used to perform blockchain voting were implemented in the library.

*   Optional build support for creating and validating traceable ring signatures was implemented.

As part of the project development, improved electrum support in `libbitcoincash` and `ElectrsCash` have been made.
